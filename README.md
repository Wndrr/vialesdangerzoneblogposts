VialesDangerZoneBlogPosts



## Ideas

### Re-write lost posts

- Comments matter !
- Naming matters & the context of code. Written once, read many times 
- Use constructors, god dammit !

### New posts

- Null isn't a value !
- n-parts tuto on .NET Core 3.1 (blazor) + Gitlab CD/CI + Kubernetes integration
- Magic values (string, int, etc) are bad and should be avoided 
- Consider your self as multiple developers, never write code assuming you'll understand it 6 month later
- Why my blog uses GIT as a database